﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        
        double buff=0;
        double rez = 0;
        int count = 0;
       
        public Form1()
        {
            InitializeComponent();
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            textBox2.Text = textBox2.Text + 1;
           
        }
        private void button2_Click(object sender, EventArgs e)
        {
            textBox2.Text = textBox2.Text + 2;
          
        }
        private void button3_Click(object sender, EventArgs e)
        {
            textBox2.Text = textBox2.Text + 3;
           
        }
        private void button4_Click(object sender, EventArgs e)
        {   
            textBox2.Text = textBox2.Text + 4;
           
        }
        private void button5_Click(object sender, EventArgs e)
        {
            textBox2.Text = textBox2.Text + 5;
           
        }
        private void button6_Click(object sender, EventArgs e)
        {
            textBox2.Text = textBox2.Text + 6;
           
        }
        private void button7_Click(object sender, EventArgs e)
        {
            textBox2.Text = textBox2.Text + 7;
           
        }
        private void button8_Click(object sender, EventArgs e)
        {
            textBox2.Text = textBox2.Text + 8;
            
        }
        private void button9_Click(object sender, EventArgs e)
        {
            textBox2.Text = textBox2.Text + 9;
            
        }
        private void button0_Click(object sender, EventArgs e)
        {
            textBox2.Text = textBox2.Text + 0;
            
        }

        private void button21_Click(object sender, EventArgs e)
        {
            textBox2.Text = textBox2.Text + ".";
           
        }

        private void button11_Click(object sender, EventArgs e) // +
        {
            buff = double.Parse(textBox2.Text);
            //buff = double.Parse(label1.Text);
            count = 1;
          
            textBox2.Clear();
        }



        private void button15_Click(object sender, EventArgs e) // ==========
        {
            izracun();
        }

        private void button12_Click(object sender, EventArgs e) // -
        {
            buff = double.Parse(textBox2.Text);
            //buff = double.Parse(label1.Text);
            count = 2;
            
            textBox2.Clear();
        }

        private void button13_Click(object sender, EventArgs e) // *
        {
            buff = double.Parse(textBox2.Text);
            //buff = double.Parse(label1.Text);
            count = 3;
           
            textBox2.Clear();
        }

        private void button14_Click(object sender, EventArgs e) // /
        {
            buff = double.Parse(textBox2.Text);
            //buff = double.Parse(label1.Text);
            count = 4;
           
            textBox2.Clear();
        }

        private void button19_Click(object sender, EventArgs e) // sqrt
        {
            buff = double.Parse(textBox2.Text);
            //buff = double.Parse(label1.Text);
            count = 5;
           
            textBox2.Clear();
        }

        private void button16_Click(object sender, EventArgs e) // pow
        {
            buff = double.Parse(textBox2.Text);
            //buff = double.Parse(label1.Text);
            count = 6;
           
            textBox2.Clear();
        }

        private void button17_Click(object sender, EventArgs e) // log
        {
            buff = double.Parse(textBox2.Text);
            //buff = double.Parse(label1.Text);
            count = 7;
            
            textBox2.Clear();
        }
       
        private void button18_Click(object sender, EventArgs e) // sin
        {
            buff = double.Parse(textBox2.Text);
            //buff = double.Parse(label1.Text);
            count = 8;
           
            textBox2.Clear();
        }

        private void button20_Click(object sender, EventArgs e) // cos
        {
            buff = double.Parse(textBox2.Text);
            //buff = double.Parse(label1.Text);
            count = 9;
          
            textBox2.Clear();

        }
        public void izracun()
        {
            switch (count)
            {
                case 1:
                    rez = buff + double.Parse(textBox2.Text);
                    label2.Text = rez.ToString();
                    break;
                case 2:
                    rez = buff - double.Parse(textBox2.Text);
                    label2.Text = rez.ToString();
                    break;
                case 3:
                    rez = buff * double.Parse(textBox2.Text);
                    label2.Text = rez.ToString();
                    break;
                case 4:
                    rez = buff / double.Parse(textBox2.Text);
                    label2.Text = rez.ToString();
                    break;
                case 5:
                    rez = Math.Sqrt(buff);
                    label2.Text = rez.ToString();
                    break;
                case 6:
                    rez = Math.Pow(buff,double.Parse(textBox2.Text));
                    label2.Text = rez.ToString();
                    break;
                case 7:
                    rez = Math.Log(buff);
                    label2.Text = rez.ToString();
                    break;
                case 8:
                    rez = Math.Sin(buff);
                    label2.Text = rez.ToString();
                    break;
                case 9:
                    rez = Math.Cos(buff);
                    label2.Text = rez.ToString();
                    break;
            }
        }

        private void button22_Click(object sender, EventArgs e)
        {
            textBox2.Clear();
            buff = 0;
            rez = 0;
            label2.Text = "";
        }
    }
}
